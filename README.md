# blue

The uncompromising Kotlin code formatter

The goal of the project is to create an ultimate formatting tool so that 
developers would no longer need to worry about formatting at all. The idea is
to provide `plug and play` tool which makes all the decisions about formatting
by itself. The project is inspired by 
the [black](https://github.com/python/black) project.

Code coverage: https://grihabor.gitlab.io/blue/

## Gradle plugin 

See gradle plugin portal for details: https://plugins.gradle.org/plugin/com.github.grihabor.blue

### Tasks defined by the plugin

| Task name      | Description                             |
|----------------|-----------------------------------------|
| **checkBlue**  | Checks files formatting with blue tool  |
| **formatBlue** | Reformats files in place with blue tool |

## CLI

### Install

```bash
curl -L --output blue-tool.zip https://dl.bintray.com/grihabor/maven/com/github/grihabor/blue/blue-tool/1.1.3/blue-tool-1.1.3.zip
unzip blue-tool.zip
rm blue-tool.zip
mv blue-* blue
blue/bin/blue --help
```
