import java.util.Properties

plugins {
  kotlin("jvm") version Blue.kotlinVersion
  id("application")
  id("jacoco")
  id("maven-publish")
  id("com.jfrog.bintray")
  id("com.github.grihabor.blue-common-plugin")
}

application {
  mainClassName = "${project.group}.Tool"
  this.applicationName = "blue"
}

repositories {
    mavenLocal()
    mavenCentral()
}


dependencies {
  "implementation"(project(":core"))
  implementation("org.jetbrains.kotlin:kotlin-stdlib:${Blue.kotlinVersion}")
  implementation("info.picocli:picocli:3.9.5")
  testImplementation("junit:junit:4.11")
  testImplementation("org.jetbrains.kotlin:kotlin-test-junit:${Blue.kotlinVersion}")
}

tasks.test {
  testLogging.showStandardStreams = true
}

jacoco {
  toolVersion = "0.8.1"
}


tasks.jacocoTestReport {
  reports {
    xml.isEnabled = false
    csv.isEnabled = false
    html.isEnabled = true
  }
}

blueCommon {
  groupId = project.group.toString()
  user = System.getenv("BINTRAY_USER")
  key = System.getenv("BINTRAY_API_KEY")
  version = project.version.toString()
  artifactId = "blue-tool"
  description = "Blue formatting tool"
}


tasks.jar {
  this.archiveBaseName.set("blue-gradle-plugin")
}

val generatedVersionDir = "${project.buildDir}/generated-version"
val versionTaskName = "generateVersionProperties"

sourceSets {
  main {
    output.dir(mapOf(Pair("builtBy", versionTaskName)), generatedVersionDir)
  }
}

task<Task>(versionTaskName){
  doLast {
    val propertiesFile = file("$generatedVersionDir/version.properties")
    propertiesFile.parentFile.mkdirs()
    val properties = Properties()
    properties.setProperty("version", rootProject.version.toString())
    propertiesFile.writer().let { properties.store(it, null) }
  }
}

tasks.named<Task>("processResources"){
  dependsOn(versionTaskName)
}
