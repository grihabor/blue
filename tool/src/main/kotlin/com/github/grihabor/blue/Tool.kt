package com.github.grihabor.blue

import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters
import java.util.*

@Command(name = "format", mixinStandardHelpOptions = true)
class Format : Runnable, SharedParameters() {

    override fun run() {
        val filenames = getFilenames()
        if (filenames.isEmpty()) {
            CommandLine(this).usage(System.err)
            return
        }
        reformatInplace(filenames, lineLengthLimit, verbose)
        printAllFilesOK()
    }
}

open class SharedParameters {
    @Option(names = ["-p", "--pattern"])
    private var pattern: String = BLUE_DEFAULT_PATTERN

    @Option(names = ["-v", "--verbose"])
    var verbose: Boolean = false

    @Option(names = ["-l", "--line-length"])
    var lineLengthLimit: Int = 90

    @Parameters
    private var filenames: Array<String> = emptyArray()

    fun getFilenames(): List<String> = searchFiles(filenames.toList(), pattern)
}

@Command(name = "check", mixinStandardHelpOptions = true)
class Check : Runnable, SharedParameters() {

    override fun run() {
        val filenames = getFilenames()
        if (filenames.isEmpty()) {
            CommandLine(this).usage(System.err)
            return
        }
        val failCount = checkFormatting(filenames, lineLengthLimit, verbose)
        if (failCount > 0) {
            System.exit(1)
        } else {
            printAllFilesOK()
        }
    }
}

class BlueProperties : CommandLine.IVersionProvider {
    private val properties: Properties
        get() {
            val properties = Properties()
            val stream =
                BlueProperties::class.java.getResource("/version.properties").openStream()
            properties.load(stream)
            return properties
        }

    override fun getVersion(): Array<String> {
        return arrayOf("blue " + this.properties.getProperty("version"))
    }
}

@Command(
    name = "blue",
    subcommands = [Format::class, Check::class],
    mixinStandardHelpOptions = true,
    versionProvider = BlueProperties::class
)
class Tool : Runnable {
    override fun run() {
        CommandLine(this).usage(System.err)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            CommandLine.run(Tool(), *args)
        }
    }
}
