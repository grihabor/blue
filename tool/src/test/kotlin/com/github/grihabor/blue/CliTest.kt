package com.github.grihabor.blue

import org.junit.Test
import kotlin.test.assertEquals
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class MatchTestResources {
    @Test
    fun TestCliHelp() {
        val bytes = ByteArrayOutputStream()
        val console = System.out
        try {
            System.setOut(PrintStream(bytes))
            Tool.main(arrayOf("--help"))
        } finally {
            System.setOut(console)
        }
        assertEquals("""
Usage: blue [-hV] [COMMAND]
  -h, --help      Show this help message and exit.
  -V, --version   Print version information and exit.
Commands:
  format
  check
            """.trim() + "\n", bytes.toString())
    }
}
