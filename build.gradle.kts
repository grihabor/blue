import kotlin.text.RegexOption
import com.github.grihabor.blue.BluePluginExtension
import junit.framework.Assert.assertEquals


buildscript {
    repositories {
        maven {
            url = uri("https://dl.bintray.com/grihabor/maven")
        }
        mavenCentral()
    }
    dependencies {
        classpath("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.8.4")
    }
}

allprojects {
    version = Blue.version
    group = "com.github.grihabor.blue"
}

plugins {
    jacoco
    id("com.github.grihabor.blue") version "0.1.0-a8"
}

repositories {
    mavenCentral()
}

fun getMainSourceSet(project: Project): SourceSet {
    val sourceSets = project.extensions.getByType(SourceSetContainer::class.java)
    val main = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)
    return main
}
evaluationDependsOnChildren()

val codeCoverageReport = "coverage"
task<JacocoReport>(codeCoverageReport) {

    val reportTasks: List<JacocoReport> = subprojects.map { subproject ->
        subproject.tasks.named<JacocoReport>("jacocoTestReport").get()
    }
    for (task in reportTasks) {
        dependsOn(task)
    }
    executionData.setFrom({
        val executionDataFiles = reportTasks.map {
            task: JacocoReport -> task.executionData.files
        }.flatten()
        executionDataFiles
    }())

    sourceDirectories.setFrom(
        subprojects.map(::getMainSourceSet).map { it.allJava }.map {it.srcDirs}.flatten()
    )
    classDirectories.setFrom(
        subprojects.map(::getMainSourceSet).map { it.output }.map {it.classesDirs}.flatten()
    )
    reports {
        xml.isEnabled = true
        xml.destination = File("$buildDir/reports/jacoco/report.xml")
        html.isEnabled = true
        html.destination = File("$buildDir/reports/jacoco/html")
        csv.isEnabled = false
    }
}

tasks.named<JacocoReport>(codeCoverageReport) {
    subprojects.forEach {subproject ->
        val task: Task = subproject.tasks.named("test").get()
        this.dependsOn(task)
    }
}


task<Task>("assemble") {
    subprojects.forEach { subproject ->
        this.dependsOn(
            subproject.tasks.named("assemble").get()
        )
    }
}

task<Task>("local") {
    subprojects.forEach { subproject ->
        this.dependsOn(
            subproject.tasks.named("publishToMavenLocal").get()
        )
    }
}

task<Task>("upload") {
    subprojects.forEach { subproject ->
        this.dependsOn(
            subproject.tasks.named("bintrayUpload").get()
        )
    }
}

val readmeFilename = "README.md"

task<Task>("checkReadmeTaskDescriptions") {
    doLast {
        val readmeLines = File(readmeFilename).readLines()
        val markdownTableRowRegex = Regex("\\|([^|]*)\\|([^|]*)\\|")
        val descriptions = readmeLines.filter {
            line -> line.matches(markdownTableRowRegex)
        }.map { line ->
            markdownTableRowRegex.matchEntire(line)
        }.map {
            it!!.groupValues
        }.filter {groupValues ->
            groupValues[1].trim().toLowerCase().contains("blue")
        }.map { groupValues ->
            groupValues.let {
                it[1].trim().trim('*') to it[2].trim()
            }
        }.toMap()
        assertEquals(Blue.checkBlueDescription, descriptions["checkBlue"])
        assertEquals(Blue.formatBlueDescription, descriptions["formatBlue"])
    }
}

task<Task>("checkReadmeCLIInstall") {
    doLast {
        val readme = File(readmeFilename).readText()
        val codeBlockRegex = Regex("```bash(.*)```", RegexOption.DOT_MATCHES_ALL)
        val script = codeBlockRegex.find(readme)!!.groupValues[1]
        val workdir = createTempDir()
        val installScriptFile = File(workdir, "install.sh")
        installScriptFile.writeText(script)
        println("--- Running shell script ---\n${script}")
        val process = (
            ProcessBuilder("/bin/sh", installScriptFile.absolutePath)
            .directory(workdir)
            .start()
        )
        process.waitFor(30, TimeUnit.SECONDS)
        assertEquals(0, process.exitValue())
    }
}

task<Task>("checkReadme") {
    dependsOn("checkReadmeTaskDescriptions")
    dependsOn("checkReadmeCLIInstall")
}

task<Task>("check") {
    dependsOn("checkReadme")
    dependsOn("checkBlue")
}

the<BluePluginExtension>().apply {
    filenames = arrayOf("gradle_plugin", "core", "buildSrc", "tool")
    lineLengthLimit = 90
}

task<Task>("version") {
    println("${Blue.version}")
}
