import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("java")
  `java-library`
  kotlin("jvm") version Blue.kotlinVersion
  jacoco
  id("maven-publish")
  id("com.jfrog.bintray")
  id("com.github.grihabor.blue-common-plugin")
}

repositories {
  mavenCentral()
  jcenter()
}

dependencies {
  compile("org.jetbrains.kotlin:kotlin-stdlib-common:${Blue.kotlinVersion}")
  compile("org.jetbrains.kotlin:kotlin-stdlib:${Blue.kotlinVersion}")
  compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  compile("org.jetbrains.kotlin:kotlin-compiler-embeddable:${Blue.kotlinVersion}")

  testImplementation("junit:junit:4.12")
  testImplementation("org.jetbrains.kotlin:kotlin-test-junit:${Blue.kotlinVersion}")
}

tasks.jar {
  this.archiveBaseName.set("blue-core")
}

tasks.test {
  testLogging.showStandardStreams = true
}

jacoco {
  toolVersion = "0.8.1"
}


tasks.named<JacocoReport>("jacocoTestReport"){
  reports {
    xml.isEnabled = false
    csv.isEnabled = false
    html.isEnabled = true
  }
}

blueCommon {
  groupId = project.group.toString()
  user = System.getenv("BINTRAY_USER")
  key = System.getenv("BINTRAY_API_KEY")
  version = project.version.toString()
  artifactId = "blue-core"
  description = "Blue formatting tool core"
}

tasks.withType<KotlinCompile>().all {
  kotlinOptions {
    jvmTarget = "1.8"
  }
}