package com.github.grihabor.blue

import com.github.grihabor.blue.kastree.Visitor
import com.github.grihabor.blue.kastree.Node
import kotlin.reflect.KClass
import kotlin.text.StringBuilder

fun writeExtrasLineEnd(v: Node, extrasMap: ExtrasManager, app: FileBuilder) {
    // Write everything after the first non-line starter/ender
    writeExtras(extrasMap.extrasAfter(v).dropWhile {
        it is Node.Extra.Comment && !it.startsLine && !it.endsLine
    }, app)
}

fun writeExtras(extras: List<Node.Extra>, app: FileBuilder) = extras.forEach {
    // A workaround to fix startsLine at the beginning of the file
    val startsFile = app.isEmpty()

    when (it) {
        is Node.Extra.BlankLines -> {
            // If there are more than one blank line - skip all except one
            if (it.count < 1) return@forEach

            val latestLine = app.removeLatestLine()
            app.line()
            app.append(latestLine)
        }
        is Node.Extra.Comment -> {
            val startsLine = it.startsLine || startsFile

            if (startsLine && it.endsLine) {
                app.lineEnd(it.text)
                app.lineBegin()
            } else {
                if (!startsLine) {
                    app.append(' ')
                }
                app.append(it.text)
                if (!it.endsLine) {
                    app.append(' ')
                }
            }
        }
    }
}

data class WriteResult(
    val text: String
)

fun CharSequence.hasLongLines(limit: Int) = lines().any {
    it.length > limit
}

open class Writer(
    var extrasMap: ExtrasManager,
    val lineLengthLimit: Int,
    val blankLineTendency: Int = 0,
    val indent: String = "",
    val canThrow: Boolean = false,
    val parentLatestLine: StringBuilder = StringBuilder()
) : Visitor() {

    private var elemsSinceLastLine = emptyList<Node>()

    private fun makeFileBuilder() = FileBuilder(beforeLineEnd = { fb: FileBuilder ->
        val elems = elemsSinceLastLine
        elemsSinceLastLine = emptyList()
        elems.forEach {
            writeExtrasLineEnd(it, extrasMap, fb)
        }
    }, startIndent = indent)

    val app: FileBuilder = makeFileBuilder()

    fun write(v: Node) {
        visit(v, v)
    }

    override fun visit(v: Node?, parent: Node) {
        visitNodeWithExtras(v, parent)
    }

    private fun write(
        v: Node,
        parent: Node,
        blankLineTendency: Int,
        canThrow: Boolean,
        parentLatestLine: StringBuilder
    ): WriteResult =
        WriteResult(
            text = Writer(
                extrasMap,
                indent = app.indent,
                blankLineTendency = blankLineTendency,
                canThrow = canThrow,
                parentLatestLine = parentLatestLine,
                lineLengthLimit = lineLengthLimit
            ).run {
                val functionList: List<(Node, Node) -> Unit>? = mapping[v::class]
                if (functionList == null) {
                    throw RuntimeException(
                        "Failed to find a visit function for this type of node: ${v::class}"
                    )
                } else {
                    if (blankLineTendency < functionList.size) {
                        functionList[blankLineTendency](v, parent)
                    } else {
                        functionList[0](v, parent)
                    }
                }

                app.toString()
            }
        )

    private fun visitNodeWithExtras(v: Node?, parent: Node) {
        if (v == null) {
            return
        }
        v.writeExtrasBefore()
        smartWrite(v, parent)
        v.writeExtrasAfter()
        v.also {
            elemsSinceLastLine += it
        }
    }

    private fun smartWrite(v: Node, parent: Node) {
        var result: WriteResult? = null
        val latestLine by lazy {
            val sb = StringBuilder(parentLatestLine)
            sb.append(app)
            return@lazy sb.getLastLine()
        }

        fun filterToken(
            node: Node,
            callback: (Node.Expr.BinaryOp.Token) -> Boolean
        ): Boolean =
            node is Node.Expr.BinaryOp &&
                node
                    .run {
                        oper is Node.Expr.BinaryOp.Oper.Token &&
                            (oper as Node.Expr.BinaryOp.Oper.Token)
                                .token
                                .run(callback)
                    }

        fun passTendencyThrough(node: Node) =
            filterToken(node) { token ->
                token == Node.Expr.BinaryOp.Token.DOT ||
                    token == Node.Expr.BinaryOp.Token.DOT_SAFE ||
                    token == Node.Expr.BinaryOp.Token.OR ||
                    token == Node.Expr.BinaryOp.Token.AND
            }

        fun isDOT(node: Node) =
            filterToken(node) { token ->
                token == Node.Expr.BinaryOp.Token.DOT ||
                    token == Node.Expr.BinaryOp.Token.DOT_SAFE
            }

        val range = if (passTendencyThrough(parent) && passTendencyThrough(v)) {
            blankLineTendency..blankLineTendency
        } else if (parent !is Node.Decl.Property && isDOT(v)) {
            /* enable chained call wrapping only for property */
            0..0
        } else 0..(mapping.getValue(v::class).size - 1)

        fun longLines(text: String): Boolean {
            val sb = StringBuilder(latestLine)
            sb.append(text)
            return sb.hasLongLines(lineLengthLimit)
        }

        val passCanThrow = when (parent) {
            is Node.Expr.Property -> false
            else -> canThrow
        }

        for (i in range) {
            try {
                result = write(
                    v,
                    parent,
                    blankLineTendency = i,
                    canThrow = i < range.last || passCanThrow,
                    parentLatestLine = latestLine
                )
            } catch (e: LineLengthLimitExceeded) {
                if (passCanThrow) {
                    throw e
                } else {
                    if (i < range.last) {
                        continue
                    } else {
                        throw RuntimeException(
                            "Unexpected behaviour: write function is supposed to return something if flag canThrow=false"
                        )
                    }
                }
            }

            if (i == range.last) {
                // explicitly break to avoid longLines execution
                break
            } else if (!longLines(result.text)) {
                break
            } else if (canThrow) {
                throw LineLengthLimitExceeded()
            }
        }
        app.append(result!!.text)
    }

    val mapping: Map<KClass<out Node>, List<(Node, Node) -> Unit>> =
        mapOf(

            Pair(Node.File::class, listOf({ node: Node, parent: Node ->
                this.visitNodeFile(node as Node.File, parent)
            })),

            Pair(Node.Package::class, listOf({ node: Node, parent: Node ->
                this.visitNodePackage(node as Node.Package, parent)
            })),

            Pair(Node.Import::class, listOf({ node: Node, parent: Node ->
                this.visitNodeImport(node as Node.Import, parent)
            })),

            Pair(Node.Decl.Structured::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclStructured(node as Node.Decl.Structured, parent)
            })),

            Pair(
                Node.Decl.Structured.Parent.CallConstructor::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclStructuredParentCallConstructor(
                            node as Node.Decl.Structured.Parent.CallConstructor,
                            parent
                        )
                    },
                    { node: Node, parent: Node ->
                        this.visitNodeDeclStructuredParentCallConstructor2(
                            node as Node.Decl.Structured.Parent.CallConstructor,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Decl.Structured.Parent.Type::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclStructuredParentType(
                            node as Node.Decl.Structured.Parent.Type,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Decl.Structured.PrimaryConstructor::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclStructuredPrimaryConstructor(
                            node as Node.Decl.Structured.PrimaryConstructor,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Decl.Init::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclInit(node as Node.Decl.Init, parent)
            })),

            Pair(Node.Decl.Func::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclFunc(node as Node.Decl.Func, parent)
            })),

            Pair(Node.Decl.Func.Header::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclFuncHeader(node as Node.Decl.Func.Header, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeDeclFuncHeader2(node as Node.Decl.Func.Header, parent)
            })),

            Pair(Node.Decl.Func.Param::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclFuncParam(node as Node.Decl.Func.Param, parent)
            })),

            Pair(Node.Decl.Func.Body.Block::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclFuncBodyBlock(node as Node.Decl.Func.Body.Block, parent)
            })),

            Pair(Node.Decl.Func.Body.Expr::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclFuncBodyExpr(node as Node.Decl.Func.Body.Expr, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeDeclFuncBodyExpr2(node as Node.Decl.Func.Body.Expr, parent)
            })),

            Pair(Node.Decl.Property::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclProperty(node as Node.Decl.Property, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeDeclProperty2(node as Node.Decl.Property, parent)
            })),

            Pair(Node.Decl.Property.Var::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclPropertyVar(node as Node.Decl.Property.Var, parent)
            })),

            Pair(
                Node.Decl.Property.Accessors::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclPropertyAccessors(
                            node as Node.Decl.Property.Accessors,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Decl.Property.Accessor.Get::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclPropertyAccessorGet(
                            node as Node.Decl.Property.Accessor.Get,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Decl.Property.Accessor.Set::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclPropertyAccessorSet(
                            node as Node.Decl.Property.Accessor.Set,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Decl.TypeAlias::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclTypeAlias(node as Node.Decl.TypeAlias, parent)
            })),

            Pair(Node.Decl.Constructor::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclConstructor(node as Node.Decl.Constructor, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeDeclConstructor2(node as Node.Decl.Constructor, parent)
            })),

            Pair(
                Node.Decl.Constructor.DelegationCall::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeDeclConstructorDelegationCall(
                            node as Node.Decl.Constructor.DelegationCall,
                            parent
                        )
                    },
                    { node: Node, parent: Node ->
                        this.visitNodeDeclConstructorDelegationCall2(
                            node as Node.Decl.Constructor.DelegationCall,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Decl.EnumEntry::class, listOf({ node: Node, parent: Node ->
                this.visitNodeDeclEnumEntry(node as Node.Decl.EnumEntry, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeDeclEnumEntry2(node as Node.Decl.EnumEntry, parent)
            })),

            Pair(Node.TypeParam::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeParam(node as Node.TypeParam, parent)
            })),

            Pair(Node.TypeConstraint::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeConstraint(node as Node.TypeConstraint, parent)
            })),

            Pair(Node.TypeRef.Paren::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefParen(node as Node.TypeRef.Paren, parent)
            })),

            Pair(Node.TypeRef.Func::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefFunc(node as Node.TypeRef.Func, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeTypeRefFunc2(node as Node.TypeRef.Func, parent)
            })),

            Pair(Node.TypeRef.Func.Param::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefFuncParam(node as Node.TypeRef.Func.Param, parent)
            })),

            Pair(Node.TypeRef.Simple::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefSimple(node as Node.TypeRef.Simple, parent)
            })),

            Pair(
                Node.TypeRef.Simple.Piece::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeTypeRefSimplePiece(
                            node as Node.TypeRef.Simple.Piece,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.TypeRef.Nullable::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefNullable(node as Node.TypeRef.Nullable, parent)
            })),

            Pair(Node.TypeRef.Dynamic::class, listOf({ node: Node, parent: Node ->
                this.visitNodeTypeRefDynamic(node as Node.TypeRef.Dynamic, parent)
            })),

            Pair(Node.Type::class, listOf({ node: Node, parent: Node ->
                this.visitNodeType(node as Node.Type, parent)
            })),

            Pair(Node.ValueArg::class, listOf({ node: Node, parent: Node ->
                this.visitNodeValueArg(node as Node.ValueArg, parent)
            })),

            Pair(Node.Expr.If::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprIf(node as Node.Expr.If, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeExprIf2(node as Node.Expr.If, parent)
            })),

            Pair(Node.Expr.Try::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprTry(node as Node.Expr.Try, parent)
            })),

            Pair(Node.Expr.Try.Catch::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprTryCatch(node as Node.Expr.Try.Catch, parent)
            })),

            Pair(Node.Expr.For::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprFor(node as Node.Expr.For, parent)
            })),

            Pair(Node.Expr.While::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhile(node as Node.Expr.While, parent)
            })),

            Pair(Node.Expr.BinaryOp::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprBinaryOp(node as Node.Expr.BinaryOp, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeExprBinaryOp2(node as Node.Expr.BinaryOp, parent)
            })),

            Pair(
                Node.Expr.BinaryOp.Oper.Infix::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprBinaryOpOperInfix(
                            node as Node.Expr.BinaryOp.Oper.Infix,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.BinaryOp.Oper.Token::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprBinaryOpOperToken(
                            node as Node.Expr.BinaryOp.Oper.Token,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Expr.UnaryOp::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprUnaryOp(node as Node.Expr.UnaryOp, parent)
            })),

            Pair(Node.Expr.UnaryOp.Oper::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprUnaryOpOper(node as Node.Expr.UnaryOp.Oper, parent)
            })),

            Pair(Node.Expr.TypeOp::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprTypeOp(node as Node.Expr.TypeOp, parent)
            })),

            Pair(Node.Expr.TypeOp.Oper::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprTypeOpOper(node as Node.Expr.TypeOp.Oper, parent)
            })),

            Pair(
                Node.Expr.DoubleColonRef.Callable::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprDoubleColonRefCallable(
                            node as Node.Expr.DoubleColonRef.Callable,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.DoubleColonRef.Class::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprDoubleColonRefClass(
                            node as Node.Expr.DoubleColonRef.Class,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.DoubleColonRef.Recv.Expr::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprDoubleColonRefRecvExpr(
                            node as Node.Expr.DoubleColonRef.Recv.Expr,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.DoubleColonRef.Recv.Type::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprDoubleColonRefRecvType(
                            node as Node.Expr.DoubleColonRef.Recv.Type,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Expr.Paren::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprParen(node as Node.Expr.Paren, parent)
            })),

            Pair(Node.Expr.StringTmpl::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprStringTmpl(node as Node.Expr.StringTmpl, parent)
            })),

            Pair(
                Node.Expr.StringTmpl.Elem.Regular::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprStringTmplElemRegular(
                            node as Node.Expr.StringTmpl.Elem.Regular,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.StringTmpl.Elem.ShortTmpl::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprStringTmplElemShortTmpl(
                            node as Node.Expr.StringTmpl.Elem.ShortTmpl,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.StringTmpl.Elem.UnicodeEsc::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprStringTmplElemUnicodeEsc(
                            node as Node.Expr.StringTmpl.Elem.UnicodeEsc,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.StringTmpl.Elem.RegularEsc::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprStringTmplElemRegularEsc(
                            node as Node.Expr.StringTmpl.Elem.RegularEsc,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Expr.StringTmpl.Elem.LongTmpl::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprStringTmplElemLongTmpl(
                            node as Node.Expr.StringTmpl.Elem.LongTmpl,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Expr.Const::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprConst(node as Node.Expr.Const, parent)
            })),

            Pair(Node.Expr.Brace.Header::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprBraceHeader(node as Node.Expr.Brace.Header, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeExprBraceHeader2(node as Node.Expr.Brace.Header, parent)
            })),
            Pair(Node.Expr.Brace::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprBrace(node as Node.Expr.Brace, parent)
            })),

            Pair(Node.Expr.Brace.Header.Param::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprBraceParam(node as Node.Expr.Brace.Header.Param, parent)
            })),

            Pair(Node.Expr.This::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprThis(node as Node.Expr.This, parent)
            })),

            Pair(Node.Expr.Super::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprSuper(node as Node.Expr.Super, parent)
            })),

            Pair(Node.Expr.When::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhen(node as Node.Expr.When, parent)
            })),

            Pair(Node.Expr.When.Entry::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhenEntry(node as Node.Expr.When.Entry, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeExprWhenEntry2(node as Node.Expr.When.Entry, parent)
            })),

            Pair(Node.Expr.When.Cond.Expr::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhenCondExpr(node as Node.Expr.When.Cond.Expr, parent)
            })),

            Pair(Node.Expr.When.Cond.In::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhenCondIn(node as Node.Expr.When.Cond.In, parent)
            })),

            Pair(Node.Expr.When.Cond.Is::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprWhenCondIs(node as Node.Expr.When.Cond.Is, parent)
            })),

            Pair(Node.Expr.Object::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprObject(node as Node.Expr.Object, parent)
            })),

            Pair(Node.Expr.Throw::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprThrow(node as Node.Expr.Throw, parent)
            })),

            Pair(Node.Expr.Return::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprReturn(node as Node.Expr.Return, parent)
            })),

            Pair(Node.Expr.Continue::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprContinue(node as Node.Expr.Continue, parent)
            })),

            Pair(Node.Expr.Break::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprBreak(node as Node.Expr.Break, parent)
            })),

            Pair(Node.Expr.CollLit::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprCollLit(node as Node.Expr.CollLit, parent)
            })),

            Pair(Node.Expr.Name::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprName(node as Node.Expr.Name, parent)
            })),

            Pair(Node.Expr.Labeled::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprLabeled(node as Node.Expr.Labeled, parent)
            })),

            Pair(Node.Expr.Annotated::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprAnnotated(node as Node.Expr.Annotated, parent)
            })),

            Pair(Node.Expr.Call::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprCall(node as Node.Expr.Call, parent)
            })),

            Pair(Node.Expr.Call.Args::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprCallArgs(node as Node.Expr.Call.Args, parent)
            }, { node: Node, parent: Node ->
                this.visitNodeExprCallArgs2(node as Node.Expr.Call.Args, parent)
            })),

            Pair(Node.Expr.Call.TypeArgs::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprCallTypeArgs(node as Node.Expr.Call.TypeArgs, parent)
            })),

            Pair(
                Node.Expr.Call.TrailLambda::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeExprCallTrailLambda(
                            node as Node.Expr.Call.TrailLambda,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Expr.ArrayAccess::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprArrayAccess(node as Node.Expr.ArrayAccess, parent)
            })),

            Pair(Node.Expr.AnonFunc::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprAnonFunc(node as Node.Expr.AnonFunc, parent)
            })),

            Pair(Node.Expr.Property::class, listOf({ node: Node, parent: Node ->
                this.visitNodeExprProperty(node as Node.Expr.Property, parent)
            })),

            Pair(Node.Block::class, listOf({ node: Node, parent: Node ->
                this.visitNodeBlock(node as Node.Block, parent)
            })),

            Pair(Node.Stmt.Decl::class, listOf({ node: Node, parent: Node ->
                this.visitNodeStmtDecl(node as Node.Stmt.Decl, parent)
            })),

            Pair(Node.Stmt.Expr::class, listOf({ node: Node, parent: Node ->
                this.visitNodeStmtExpr(node as Node.Stmt.Expr, parent)
            })),

            Pair(
                Node.Modifier.AnnotationSet::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeModifierAnnotationSet(
                            node as Node.Modifier.AnnotationSet,
                            parent
                        )
                    }
                )
            ),

            Pair(
                Node.Modifier.AnnotationSet.Annotation::class,
                listOf(
                    { node: Node, parent: Node ->
                        this.visitNodeModifierAnnotationSetAnnotation(
                            node as Node.Modifier.AnnotationSet.Annotation,
                            parent
                        )
                    },
                    { node: Node, parent: Node ->
                        this.visitNodeModifierAnnotationSetAnnotation2(
                            node as Node.Modifier.AnnotationSet.Annotation,
                            parent
                        )
                    }
                )
            ),

            Pair(Node.Modifier.Lit::class, listOf({ node: Node, parent: Node ->
                this.visitNodeModifierLit(node as Node.Modifier.Lit, parent)
            }))
        )

    protected open fun visitNodeFile(v: Node.File, parent: Node) {
        v.apply {
            if (anns.isNotEmpty()) {
                childAnns()
                app.line()
                app.line()
            }

            if (pkg != null) {
                extrasMap.forceBlankLinesBefore(
                    pkg as Node,
                    policy = Policy.ForceBasedOnComments
                )
                extrasMap.forceBlankLinesAfter(pkg as Node, policy = Policy.ForceZero)
                if (imports.isNotEmpty()) {
                    extrasMap.forceBlankLinesBefore(imports[0], policy = Policy.ForceOne)
                    extrasMap.forceBlankLinesAfter(
                        imports[imports.size - 1],
                        policy = Policy.ForceZero
                    )
                }
                if (decls.isNotEmpty()) {
                    extrasMap.forceBlankLinesBefore(decls[0], policy = Policy.ForceOne)
                }
            }

            childrenLines(pkg)
            childrenLines(imports)
            childrenLines(decls)
        }
    }

    protected open fun visitNodePackage(v: Node.Package, parent: Node) {
        v.apply {
            childMods()
            app.append("package ").appendNames(names, ".")
        }
    }

    protected open fun visitNodeImport(v: Node.Import, parent: Node) {
        v.apply {
            app.append("import ").appendNames(names, ".")
            if (wildcard) {
                app.append(".*")
            } else if (alias != null) {
                app.append(" as ").appendName(alias.toString())
            }
        }
    }

    protected open fun visitNodeDeclStructured(v: Node.Decl.Structured, parent: Node) {
        v.apply {
            childMods()
            app.append(when (form) {
                Node.Decl.Structured.Form.CLASS -> "class"
                Node.Decl.Structured.Form.ENUM_CLASS -> "enum class"
                Node.Decl.Structured.Form.INTERFACE -> "interface"
                Node.Decl.Structured.Form.OBJECT -> "object"
                Node.Decl.Structured.Form.COMPANION_OBJECT -> "companion object"
            })
            if (form != Node.Decl.Structured.Form.COMPANION_OBJECT || name != "Companion") {
                app.append(' ').appendName(name)
            }
            bracketedChildren(typeParams)
            children(primaryConstructor)
            if (parents.isNotEmpty()) {
                app.append(" : ")
                children(parentAnns)
                children(parents, ", ")
            }
            childTypeConstraints(typeConstraints)
            if (members.isNotEmpty()) app.lineEnd(" {").indented {
                // First, do all the enum entries if there are any
                val enumEntries = members.map {
                    it as? Node.Decl.EnumEntry
                }.takeWhile {
                    it != null
                }
                enumEntries.forEachIndexed { index, enumEntry ->
                    app.lineBegin().also {
                        children(enumEntry)
                    }
                    when (index) {
                        members.size - 1 -> app.lineEnd()
                        enumEntries.size - 1 -> app.lineEnd(";").line()
                        else -> app.lineEnd(",")
                    }
                }
                // Now the rest of the members
                childrenLines(members.drop(enumEntries.size))
            }.lineBegin("}")

            // As a special case, if an object is nameless and bodyless, we should give it an empty body
            // to avoid ambiguities with the next item
            // See: https://youtrack.jetbrains.com/issue/KT-25581
            if ((form == Node.Decl.Structured.Form.COMPANION_OBJECT ||
                form == Node.Decl.Structured.Form.OBJECT) &&
                name == "Companion" &&
                members
                    .isEmpty()) {
                app.append("{}")
            }
        }
    }

    protected open fun visitNodeDeclStructuredParentCallConstructor(
        v: Node.Decl.Structured.Parent.CallConstructor,
        parent: Node
    ) {
        v.apply {
            children(type)
            parenChildren(args, newline = false)
        }
    }

    protected open fun visitNodeDeclStructuredParentCallConstructor2(
        v: Node.Decl.Structured.Parent.CallConstructor,
        parent: Node
    ) {
        v.apply {
            children(type)
            parenChildren(args, newline = true)
        }
    }

    protected open fun visitNodeDeclStructuredParentType(
        v: Node.Decl.Structured.Parent.Type,
        parent: Node
    ) {
        v.apply {
            children(type)
            if (by != null) app.append(" by ").also {
                children(by)
            }
        }
    }

    protected open fun visitNodeDeclStructuredPrimaryConstructor(
        v: Node.Decl.Structured.PrimaryConstructor,
        parent: Node
    ) {
        v.apply {
            if (mods.isNotEmpty()) app.append(" ").also {
                childMods(newlines = false)
                app.append("constructor")
            }
            parenChildren(params, newline = true)
        }
    }

    protected open fun visitNodeDeclInit(v: Node.Decl.Init, parent: Node) {
        v.apply {
            app.append("init ").also {
                children(block)
            }
        }
    }

    protected open fun Node.Decl.Func.Header.renderName() {
        app.append("fun")
        if (name != null || typeParams.isNotEmpty() || receiverType != null) {
            app.append(' ')
        }
        bracketedChildren(typeParams, " ")
        if (receiverType != null) {
            children(receiverType)
            app.append(".")
        }
        name?.also {
            app.appendName(it)
        }
        bracketedChildren(paramTypeParams)
    }

    protected open fun visitNodeDeclFuncHeader(v: Node.Decl.Func.Header, parent: Node) {
        v.apply {
            renderName()
            parenChildren(params, newline = false)
            if (type != null) app.append(": ").also {
                children(type)
            }
            childTypeConstraints(typeConstraints)
        }
    }

    protected open fun visitNodeDeclFuncHeader2(v: Node.Decl.Func.Header, parent: Node) {
        v.apply {
            renderName()
            parenChildren(params, newline = true)
            if (type != null) app.append(": ").also {
                children(type)
            }
            childTypeConstraints(typeConstraints)
        }
    }

    protected open fun visitNodeDeclFunc(v: Node.Decl.Func, parent: Node) {
        v.apply {
            childMods()
            children(header)
            if (body != null) app.append(' ').also {
                children(body)
            }
        }
    }

    protected open fun visitNodeDeclFuncParam(v: Node.Decl.Func.Param, parent: Node) {
        v.apply {
            if (mods.isNotEmpty()) childMods(newlines = true)
            if (readOnly == true) {
                app.append("val ")
            } else {
                if (readOnly == false) app.append("var ")
            }
            app.appendName(name)
            if (type != null) app.append(": ").also {
                children(type)
            }
            if (default != null) app.append(" = ").also {
                children(default)
            }
        }
    }

    protected open fun visitNodeDeclFuncBodyBlock(
        v: Node.Decl.Func.Body.Block,
        parent: Node
    ) {
        v.apply {
            children(block)
        }
    }

    protected open fun visitNodeDeclFuncBodyExpr(
        v: Node.Decl.Func.Body.Expr,
        parent: Node
    ) {
        v.apply {
            app.append("= ").also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeDeclFuncBodyExpr2(
        v: Node.Decl.Func.Body.Expr,
        parent: Node
    ) {
        v.apply {
            app.append("=").lineEnd().indented {
                app.lineBegin()
                children(expr)
            }
        }
    }

    protected open fun Node.Decl.Property.renderAccessors() {
        if (accessors != null) {
            app.lineEnd().indented {
                app.lineBegin()
                children(accessors)
            }
        }
    }

    protected open fun Node.Decl.Property.renderHeader() {
        childMods()
        app.append(if (readOnly) "val " else "var ")
        bracketedChildren(typeParams, " ")
        if (receiverType != null) {
            children(receiverType)
            app.append('.')
        }
        childVars(vars)
        childTypeConstraints(typeConstraints)
    }

    protected open fun visitNodeDeclProperty(v: Node.Decl.Property, parent: Node) {
        v.run {
            renderHeader()
            if (expr != null) {
                if (delegated) {
                    app.append(" by ")
                    children(expr)
                } else {
                    app.append(" = ")
                    children(expr)
                }
            }
            renderAccessors()
        }
    }

    protected open fun visitNodeDeclProperty2(v: Node.Decl.Property, parent: Node) {
        v.run {
            renderHeader()
            if (expr != null) {
                if (delegated) {
                    app.append(" by ")
                    children(expr)
                } else {
                    app.lineEnd(" =").indented {
                        app.lineBegin()
                        children(expr)
                    }
                }
            }
            renderAccessors()
        }
    }

    protected open fun visitNodeDeclPropertyVar(v: Node.Decl.Property.Var, parent: Node) {
        v.apply {
            app.appendName(name)
            if (type != null) app.append(": ").also {
                children(type)
            }
        }
    }

    protected open fun visitNodeDeclPropertyAccessors(
        v: Node.Decl.Property.Accessors,
        parent: Node
    ) {
        v.apply {
            children(first)
            if (second != null) {
                app.lineEnd().lineBegin()
                children(second)
            }
        }
    }

    protected open fun visitNodeDeclPropertyAccessorGet(
        v: Node.Decl.Property.Accessor.Get,
        parent: Node
    ) {
        v.apply {
            childMods()
            app.append("get")
            if (body != null) {
                app.append("()")
                if (type != null) app.append(": ").also {
                    children(type)
                }
                app.append(' ').also {
                    children(body)
                }
            }
        }
    }

    protected open fun visitNodeDeclPropertyAccessorSet(
        v: Node.Decl.Property.Accessor.Set,
        parent: Node
    ) {
        v.apply {
            childMods()
            app.append("set")
            if (body != null) {
                app.append('(')
                childMods(paramMods, newlines = false)
                app.appendName(
                    paramName ?: error("Missing setter param name when body present")
                )
                if (paramType != null) app.append(": ").also {
                    children(paramType)
                }
                app.append(") ")
                children(body)
            }
        }
    }

    protected open fun visitNodeDeclTypeAlias(v: Node.Decl.TypeAlias, parent: Node) {
        v.apply {
            childMods()
            app.append("typealias ").appendName(name)
            bracketedChildren(typeParams)
            app.append(" = ")
            children(type)
        }
    }

    protected open fun visitNodeDeclConstructor(v: Node.Decl.Constructor, parent: Node) {
        v.apply {
            childMods()
            app.append("constructor")
            parenChildren(params, newline = false)
            renderBody()
        }
    }

    protected open fun Node.Decl.Constructor.renderBody() {
        if (delegationCall != null) app.append(" : ").also {
            children(delegationCall)
        }
        if (block != null) app.append(' ').also {
            children(block)
        }
    }

    protected open fun visitNodeDeclConstructor2(v: Node.Decl.Constructor, parent: Node) {
        v.apply {
            childMods()
            app.append("constructor")
            parenChildren(params, newline = true)
            renderBody()
        }
    }

    protected open fun visitNodeDeclConstructorDelegationCall(
        v: Node.Decl.Constructor.DelegationCall,
        parent: Node
    ) {
        v.apply {
            app.append(target.name.toLowerCase()).also {
                parenChildren(args, newline = false)
            }
        }
    }

    protected open fun visitNodeDeclConstructorDelegationCall2(
        v: Node.Decl.Constructor.DelegationCall,
        parent: Node
    ) {
        v.apply {
            app.append(target.name.toLowerCase()).also {
                parenChildren(args, newline = true)
            }
        }
    }

    protected open fun visitNodeDeclEnumEntry(v: Node.Decl.EnumEntry, parent: Node) {
        v.apply {
            childMods()
            app.appendName(name)
            if (args.isNotEmpty()) parenChildren(args, newline = false)
            if (members.isNotEmpty()) app.lineEnd(" {").indented {
                childrenLines(members)
            }.lineBegin("}")
        }
    }

    protected open fun visitNodeDeclEnumEntry2(v: Node.Decl.EnumEntry, parent: Node) {
        v.apply {
            childMods()
            app.appendName(name)
            if (args.isNotEmpty()) parenChildren(args, newline = true)
            if (members.isNotEmpty()) app.lineEnd(" {").indented {
                childrenLines(members)
            }.lineBegin("}")
        }
    }

    protected open fun visitNodeTypeParam(v: Node.TypeParam, parent: Node) {
        v.apply {
            childMods(newlines = false)
            app.appendName(name)
            if (type != null) app.append(" : ").also {
                children(type)
            }
        }
    }

    protected open fun visitNodeTypeConstraint(v: Node.TypeConstraint, parent: Node) {
        v.apply {
            childAnns(sameLine = true)
            app.apply {
                appendName(name)
                append(": ")
                children(type)
            }
        }
    }

    protected open fun visitNodeTypeRefParen(v: Node.TypeRef.Paren, parent: Node) {
        v.apply {
            app.append('(').also {
                childModsBeforeType(type).also {
                    children(type)
                }
            }.append(')')
        }
    }

    protected open fun visitNodeTypeRefFunc(v: Node.TypeRef.Func, parent: Node) {
        v.apply {
            if (receiverType != null) {
                children(receiverType)
                app.append('.')
            }
            parenChildren(params, newline = false)
            app.append(" -> ").also {
                children(type)
            }
        }
    }

    protected open fun visitNodeTypeRefFunc2(v: Node.TypeRef.Func, parent: Node) {
        v.apply {
            if (receiverType != null) {
                children(receiverType)
                app.append('.')
            }
            parenChildren(params, newline = true)
            app.append(" -> ").also {
                children(type)
            }
        }
    }

    protected open fun visitNodeTypeRefFuncParam(v: Node.TypeRef.Func.Param, parent: Node) {
        v.apply {
            if (name != null) app.appendName(name.toString()).append(": ")
            children(type)
        }
    }

    protected open fun visitNodeTypeRefSimple(v: Node.TypeRef.Simple, parent: Node) {
        v.apply {
            children(pieces, ".")
        }
    }

    protected open fun visitNodeTypeRefSimplePiece(
        v: Node.TypeRef.Simple.Piece,
        parent: Node
    ) {
        v.apply {
            app.appendName(name).also {
                bracketedChildren(typeParams)
            }
        }
    }

    protected open fun visitNodeTypeRefNullable(v: Node.TypeRef.Nullable, parent: Node) {
        v.apply {
            children(type)
            app.append('?')
        }
    }

    protected open fun visitNodeTypeRefDynamic(v: Node.TypeRef.Dynamic, parent: Node) {
        v.apply {
            app.append("dynamic")
        }
    }

    protected open fun visitNodeType(v: Node.Type, parent: Node) {
        v.apply {
            childModsBeforeType(ref).also {
                children(ref)
            }
        }
    }

    protected open fun visitNodeValueArg(v: Node.ValueArg, parent: Node) {
        v.apply {
            if (name != null) app.appendName(name.toString()).append(" = ")
            if (asterisk) app.append('*')
            children(expr)
        }
    }

    protected open fun visitNodeExprIf(v: Node.Expr.If, parent: Node) {
        v.apply {
            app.append("if (").also {
                children(expr)
            }.append(") ")
            children(body)
            if (elseBody != null) app.append(" else ").also {
                children(elseBody)
            }
        }
    }

    protected open fun withBrace(expr: Node.Expr): Node.Expr.Brace {
        return Node.Expr.Brace(
            header = Node.Expr.Brace.Header(params = emptyList()),
            block = Node.Block(stmts = listOf(Node.Stmt.Expr(expr)))
        )
    }

    protected open fun visitNodeExprIf2(v: Node.Expr.If, parent: Node) {
        v.apply {
            app.append("if (").also {
                children(expr)
            }.append(") ")
            fun render(body_: Node.Expr) =
                if (body_ is Node.Expr.Brace ||
                    (body_ is Node.Expr.Annotated && body_.expr is Node.Expr.Brace)) {
                    children(body_)
                } else {
                    children(withBrace(body_))
                }

            render(body)
            if (elseBody != null) app.append(" else ").also {
                render(elseBody as Node.Expr)
            }
        }
    }

    protected open fun visitNodeExprTry(v: Node.Expr.Try, parent: Node) {
        v.apply {
            app.append("try ")
            children(block)
            if (catches.isNotEmpty()) children(catches, " ", prefix = " ")
            if (finallyBlock != null) app.append(" finally ").also {
                children(finallyBlock)
            }
        }
    }

    protected open fun visitNodeExprTryCatch(v: Node.Expr.Try.Catch, parent: Node) {
        v.apply {
            app.append("catch (")
            childAnns(sameLine = true)
            app.appendName(varName).append(": ").also {
                children(varType)
            }.append(") ")
            children(block)
        }
    }

    protected open fun visitNodeExprFor(v: Node.Expr.For, parent: Node) {
        v.apply {
            app.append("for (")
            childAnns(sameLine = true)
            childVars(vars)
            app.append(" in ").also {
                children(inExpr)
            }.append(") ")
            children(body)
        }
    }

    protected open fun visitNodeExprWhile(v: Node.Expr.While, parent: Node) {
        v.apply {
            if (!doWhile) app.append("while (").also {
                children(expr)
            }.append(") ").also {
                children(body)
            } else app.append("do ").also {
                children(body)
            }.append(" while (").also {
                children(expr)
            }.append(')')
        }
    }

    protected open fun visitNodeExprBinaryOp(v: Node.Expr.BinaryOp, parent: Node) {
        v.apply {
            val isToken = oper is Node.Expr.BinaryOp.Oper.Token

            data class BinaryOpSettings(
                val sepLeft: String,
                val sepRight: String
            )

            val args: BinaryOpSettings =
                if (isToken) {
                    (oper as Node.Expr.BinaryOp.Oper.Token).token.let { token ->
                        when (token) {
                            Node.Expr.BinaryOp.Token.RANGE -> BinaryOpSettings("", "")
                            Node.Expr.BinaryOp.Token.DOT, Node.Expr.BinaryOp.Token.DOT_SAFE -> {
                                BinaryOpSettings("", "")
                            }
                            Node.Expr.BinaryOp.Token.OR, Node.Expr.BinaryOp.Token.AND -> {
                                BinaryOpSettings(" ", " ")
                            }
                            else -> BinaryOpSettings(" ", " ")
                        }
                    }
                } else {
                    BinaryOpSettings(" ", " ")
                }

            visit(lhs, this)

            app.append(args.sepLeft)
            visit(oper, this)
            app.append(args.sepRight)
            visit(rhs, this)
        }
    }

    protected open fun visitNodeExprBinaryOp2(v: Node.Expr.BinaryOp, parent: Node) {
        v.apply {
            val isToken = oper is Node.Expr.BinaryOp.Oper.Token

            data class BinaryOpSettings(
                val sepLeft: String,
                val sepRight: String,
                val newlineLeft: Boolean,
                val newlineRight: Boolean
            )

            val args: BinaryOpSettings =
                if (isToken) {
                    (oper as Node.Expr.BinaryOp.Oper.Token).token.let { token ->
                        when (token) {
                            Node.Expr.BinaryOp.Token.RANGE -> {
                                BinaryOpSettings("", "", false, false)
                            }
                            Node.Expr.BinaryOp.Token.DOT, Node.Expr.BinaryOp.Token.DOT_SAFE -> {
                                BinaryOpSettings("", "", true, false)
                            }
                            Node.Expr.BinaryOp.Token.OR, Node.Expr.BinaryOp.Token.AND -> {
                                BinaryOpSettings(" ", " ", false, true)
                            }
                            else -> BinaryOpSettings(" ", " ", false, false)
                        }
                    }
                } else {
                    BinaryOpSettings(" ", " ", false, false)
                }

            visit(lhs, this)

            if (args.newlineLeft && args.newlineRight) {
                throw NotImplementedError()
            } else if (args.newlineLeft && !args.newlineRight) {
                app.indented {
                    app.lineEnd().lineBegin()
                    visit(oper, this)
                    app.append(args.sepRight)
                    visit(rhs, this)
                }
            } else if (!args.newlineLeft && args.newlineRight) {
                app.append(args.sepLeft)
                visit(oper, this)
                app.indented {
                    app.lineEnd().lineBegin()
                    visit(rhs, this)
                }
            } else {
                app.append(args.sepLeft)
                visit(oper, this)
                app.append(args.sepRight)
                visit(rhs, this)
            }
        }
    }

    protected open fun visitNodeExprBinaryOpOperInfix(
        v: Node.Expr.BinaryOp.Oper.Infix,
        parent: Node
    ) {
        v.apply {
            app.append(str)
        }
    }

    protected open fun visitNodeExprBinaryOpOperToken(
        v: Node.Expr.BinaryOp.Oper.Token,
        parent: Node
    ) {
        v.apply {
            app.append(token.str)
        }
    }

    protected open fun visitNodeExprUnaryOp(v: Node.Expr.UnaryOp, parent: Node) {
        v.apply {
            if (prefix) {
                children(oper)
                children(expr)
            } else {
                children(expr)
                children(oper)
            }
        }
    }

    protected open fun visitNodeExprUnaryOpOper(v: Node.Expr.UnaryOp.Oper, parent: Node) {
        v.apply {
            app.append(token.str)
        }
    }

    protected open fun visitNodeExprTypeOp(v: Node.Expr.TypeOp, parent: Node) {
        v.apply {
            children(listOf(lhs, oper, rhs), " ")
        }
    }

    protected open fun visitNodeExprTypeOpOper(v: Node.Expr.TypeOp.Oper, parent: Node) {
        v.apply {
            app.append(token.str)
        }
    }

    protected open fun visitNodeExprDoubleColonRefCallable(
        v: Node.Expr.DoubleColonRef.Callable,
        parent: Node
    ) {
        v.apply {
            if (recv != null) children(recv)
            app.append("::").appendName(name)
        }
    }

    protected open fun visitNodeExprDoubleColonRefClass(
        v: Node.Expr.DoubleColonRef.Class,
        parent: Node
    ) {
        v.apply {
            if (recv != null) children(recv)
            app.append("::class")
        }
    }

    protected open fun visitNodeExprDoubleColonRefRecvExpr(
        v: Node.Expr.DoubleColonRef.Recv.Expr,
        parent: Node
    ) {
        v.apply {
            children(expr)
        }
    }

    protected open fun visitNodeExprDoubleColonRefRecvType(
        v: Node.Expr.DoubleColonRef.Recv.Type,
        parent: Node
    ) {
        v.apply {
            children(type)
            app.append("?".repeat(questionMarks))
        }
    }

    protected open fun visitNodeExprParen(v: Node.Expr.Paren, parent: Node) {
        v.apply {
            app.append('(').also {
                children(expr)
            }.append(')')
        }
    }

    protected open fun visitNodeExprStringTmpl(v: Node.Expr.StringTmpl, parent: Node) {
        v.apply {
            if (raw) app.append("\"\"\"").also {
                children(elems)
            }.append("\"\"\"") else app.append('"').also {
                children(elems)
            }.append('"')
        }
    }

    protected open fun visitNodeExprStringTmplElemRegular(
        v: Node.Expr.StringTmpl.Elem.Regular,
        parent: Node
    ) {
        v.apply {
            app.append(str)
        }
    }

    protected open fun visitNodeExprStringTmplElemShortTmpl(
        v: Node.Expr.StringTmpl.Elem.ShortTmpl,
        parent: Node
    ) {
        v.apply {
            app.append('$').appendName(str)
        }
    }

    protected open fun visitNodeExprStringTmplElemUnicodeEsc(
        v: Node.Expr.StringTmpl.Elem.UnicodeEsc,
        parent: Node
    ) {
        v.apply {
            app.append("\\u").append(digits)
        }
    }

    protected open fun visitNodeExprStringTmplElemRegularEsc(
        v: Node.Expr.StringTmpl.Elem.RegularEsc,
        parent: Node
    ) {
        v.apply {
            app.append('\\').append(when (char) {
                '\b' -> 'b'
                '\n' -> 'n'
                '\t' -> 't'
                '\r' -> 'r'
                else -> char
            })
        }
    }

    protected open fun visitNodeExprStringTmplElemLongTmpl(
        v: Node.Expr.StringTmpl.Elem.LongTmpl,
        parent: Node
    ) {
        v.apply {
            app.append("\${").also {
                children(expr)
            }.append('}')
        }
    }

    protected open fun visitNodeExprConst(v: Node.Expr.Const, parent: Node) {
        v.apply {
            app.append(value)
        }
    }

    protected open fun visitNodeExprBrace(v: Node.Expr.Brace, parent: Node) {
        v.apply {
            app.append('{')
            children(header)
            children(block)
            app.append('}')
        }
    }

    protected open fun visitNodeExprBraceHeader(v: Node.Expr.Brace.Header, parent: Node) {
        v.apply {
            if (params.isNotEmpty()) {
                app.append(' ')
                children(params, ", ", "", " ->")
            }
        }
    }
    protected open fun visitNodeExprBraceHeader2(v: Node.Expr.Brace.Header, parent: Node) {
        v.apply {
            if (params.isNotEmpty()) {
                children(params, ",", "", "${app.indentation}->", newline = true)
            }
        }
    }

    protected open fun visitNodeExprBraceParam(
        v: Node.Expr.Brace.Header.Param,
        parent: Node
    ) {
        v.apply {
            childVars(vars)
            if (destructType != null) app.append(": ").also {
                children(destructType)
            }
        }
    }

    protected open fun visitNodeExprThis(v: Node.Expr.This, parent: Node) {
        v.apply {
            app.append("this")
            if (label != null) app.append('@').appendName(label.toString())
        }
    }

    protected open fun visitNodeExprSuper(v: Node.Expr.Super, parent: Node) {
        v.apply {
            app.append("super")
            if (typeArg != null) app.append('<').also {
                children(typeArg)
            }.append('>')
            if (label != null) app.append('@').appendName(label.toString())
        }
    }

    protected open fun visitNodeExprWhen(v: Node.Expr.When, parent: Node) {
        v.apply {
            app.append("when")
            if (expr != null) app.append(" (").also {
                children(expr)
            }.append(')')
            app.lineEnd(" {").indented {
                childrenLines(entries)
            }.lineBegin("}")
        }
    }

    protected open fun visitNodeExprWhenEntry(v: Node.Expr.When.Entry, parent: Node) {
        v.apply {
            if (conds.isEmpty()) app.append("else") else children(conds, ", ")
            app.append(" -> ").also {
                children(body)
            }
        }
    }

    protected open fun visitNodeExprWhenEntry2(v: Node.Expr.When.Entry, parent: Node) {
        v.apply {
            if (conds.isEmpty()) app.append("else") else children(conds, ", ")
            app.append(" -> ").also {
                if (body is Node.Expr.Brace ||
                    (body is Node.Expr.Annotated && body.expr is Node.Expr.Brace)) {
                    children(body)
                } else {
                    children(withBrace(body))
                }
            }
        }
    }

    protected open fun visitNodeExprWhenCondExpr(
        v: Node.Expr.When.Cond.Expr,
        parent: Node
    ) {
        v.apply {
            children(expr)
        }
    }

    protected open fun visitNodeExprWhenCondIn(v: Node.Expr.When.Cond.In, parent: Node) {
        v.apply {
            if (not) app.append('!')
            app.append("in ").also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeExprWhenCondIs(v: Node.Expr.When.Cond.Is, parent: Node) {
        v.apply {
            if (not) app.append('!')
            app.append("is ").also {
                children(type)
            }
        }
    }

    protected open fun visitNodeExprObject(v: Node.Expr.Object, parent: Node) {
        v.apply {
            app.append("object")
            if (parents.isNotEmpty()) app.append(" : ").also {
                children(parents, ", ")
            }
            if (members.isEmpty()) app.append(" {}") else app.lineEnd(" {").indented {
                childrenLines(members)
            }.lineBegin("}")
        }
    }

    protected open fun visitNodeExprThrow(v: Node.Expr.Throw, parent: Node) {
        v.apply {
            app.append("throw ").also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeExprReturn(v: Node.Expr.Return, parent: Node) {
        v.apply {
            app.append("return")
            if (label != null) app.append('@').appendName(label.toString())
            if (expr != null) app.append(' ').also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeExprContinue(v: Node.Expr.Continue, parent: Node) {
        v.apply {
            app.append("continue")
            if (label != null) app.append('@').appendName(label.toString())
        }
    }

    protected open fun visitNodeExprBreak(v: Node.Expr.Break, parent: Node) {
        v.apply {
            app.append("break")
            if (label != null) app.append('@').appendName(label.toString())
        }
    }

    protected open fun visitNodeExprCollLit(v: Node.Expr.CollLit, parent: Node) {
        v.apply {
            children(exprs, ", ", "[", "]")
        }
    }

    protected open fun visitNodeExprName(v: Node.Expr.Name, parent: Node) {
        v.apply {
            app.appendName(name)
        }
    }

    protected open fun visitNodeExprLabeled(v: Node.Expr.Labeled, parent: Node) {
        v.apply {
            app.appendName(label).append("@ ").also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeExprAnnotated(v: Node.Expr.Annotated, parent: Node) {
        v.apply {
            childAnnsBeforeExpr(expr).also {
                children(expr)
            }
        }
    }

    protected open fun visitNodeExprCallTypeArgs(v: Node.Expr.Call.TypeArgs, parent: Node) {
        v.apply {
            bracketedChildren(args)
        }
    }

    protected open fun visitNodeExprCallArgs(v: Node.Expr.Call.Args, parent: Node) {
        v.apply {
            if (args.isNotEmpty() || (parent as Node.Expr.Call).lambda == null) {
                parenChildren(args, newline = false)
            }
        }
    }
    protected open fun visitNodeExprCallArgs2(v: Node.Expr.Call.Args, parent: Node) {
        v.apply {
            if (args.isNotEmpty() || (parent as Node.Expr.Call).lambda == null) {
                parenChildren(args, newline = true)
            }
        }
    }

    protected open fun visitNodeExprCall(v: Node.Expr.Call, parent: Node) {
        v.apply {
            children(expr)
            children(typeArgs)
            children(args)
            if (lambda != null) {
                app.append(' ').also {
                    children(lambda)
                }
            }
        }
    }

    protected open fun visitNodeExprCallTrailLambda(
        v: Node.Expr.Call.TrailLambda,
        parent: Node
    ) {
        v.apply {
            if (anns.isNotEmpty()) {
                childAnns(sameLine = true)
                app.append(' ')
            }
            if (label != null) app.appendName(label.toString()).append("@ ")
            children(func)
        }
    }

    protected open fun visitNodeExprArrayAccess(v: Node.Expr.ArrayAccess, parent: Node) {
        v.apply {
            children(expr)
            children(indices, ", ", "[", "]")
        }
    }

    protected open fun visitNodeExprAnonFunc(v: Node.Expr.AnonFunc, parent: Node) {
        v.apply {
            children(func)
        }
    }

    protected open fun visitNodeExprProperty(v: Node.Expr.Property, parent: Node) {
        v.apply {
            children(decl)
        }
    }

    protected open fun visitNodeBlock(v: Node.Block, parent: Node) {
        v.apply {
            if (parent is Node.Expr.Brace) {
                // Special case, no braces if the parent is a brace
                if (stmts.isNotEmpty()) {
                    app.lineEnd().indented {
                        childrenLines(stmts)
                    }.lineBegin()
                }
            } else if (stmts.isEmpty() && parent !is Node.Expr.Try.Catch) {
                app.append("{}")
            } else {
                app.lineEnd("{").indented {
                    childrenLines(stmts)
                }.lineBegin("}")
            }
        }
    }

    protected open fun visitNodeStmtDecl(v: Node.Stmt.Decl, parent: Node) {
        v.apply {
            children(decl)
        }
    }

    protected open fun visitNodeStmtExpr(v: Node.Stmt.Expr, parent: Node) {
        v.apply {
            children(expr)
        }
    }

    protected open fun visitNodeModifierAnnotationSet(
        v: Node.Modifier.AnnotationSet,
        parent: Node
    ) {
        v.apply {
            app.append('@')
            if (target != null) {
                val target_ = target as Node.Modifier.AnnotationSet.Target
                app.append(target_.name.toLowerCase()).append(':')
            }
            if (anns.size == 1) children(anns) else children(anns, " ", "[", "]")
        }
    }

    protected open fun visitNodeModifierAnnotationSetAnnotation(
        v: Node.Modifier.AnnotationSet.Annotation,
        parent: Node
    ) {
        v.apply {
            app.appendNames(names, ".")
            bracketedChildren(typeArgs)
            if (args.isNotEmpty()) parenChildren(args, newline = false)
        }
    }

    protected open fun visitNodeModifierAnnotationSetAnnotation2(
        v: Node.Modifier.AnnotationSet.Annotation,
        parent: Node
    ) {
        v.apply {
            app.appendNames(names, ".")
            bracketedChildren(typeArgs)
            if (args.isNotEmpty()) parenChildren(args, newline = true)
        }
    }

    protected open fun visitNodeModifierLit(v: Node.Modifier.Lit, parent: Node) {
        v.apply {
            app.append(keyword.name.toLowerCase())
        }
    }

    protected open fun Node.writeExtrasBefore() {
        // Write everything before
        val extras = extrasMap.extrasBefore(this)
        writeExtras(extras.toList(), app)
    }

    protected open fun Node.writeExtrasAfter() {
        // Write everything after that doesn't start a line or end a line

        writeExtras(extrasMap.extrasAfter(this).takeWhile {
            it is Node.Extra.Comment && !it.startsLine && !it.endsLine
        }, app)
    }

    protected fun Node.childTypeConstraints(v: List<Node.TypeConstraint>) =
        this@Writer.also {
            if (v.isNotEmpty()) app.append(" where ").also {
                children(v, ", ")
            }
        }

    protected fun Node.childVars(vars: List<Node.Decl.Property.Var?>) =
        if (vars.size == 1) {
            if (vars.single() == null) app.append('_') else children(vars)
        } else {
            app.append('(')
            vars.forEachIndexed { index, v ->
                if (v == null) app.append('_') else children(v)
                if (index < vars.size - 1) app.append(", ")
            }
            app.append(')')
        }

    // Ends with newline (or space if sameLine) if there are any
    protected fun Node.WithAnnotations.childAnns(sameLine: Boolean = false) =
        this@Writer.also {
            if (anns.isNotEmpty()) (this@childAnns as Node).apply {
                if (sameLine) children(anns, " ", "", " ") else anns.forEach { ann ->
                    app.lineBegin().also {
                        children(ann)
                    }.lineEnd()
                }
            }
        }

    protected fun Node.WithAnnotations.childAnnsBeforeExpr(expr: Node.Expr) =
        this@Writer.also {
            if (anns.isNotEmpty()) {
                // As a special case, if there is a trailing annotation with no args and expr is paren,
                // then we need to add an empty set of parens ourselves
                val lastAnn = anns.lastOrNull()?.anns?.singleOrNull()?.takeIf {
                    it.args.isEmpty()
                }
                val shouldAddParens = lastAnn != null && expr is Node.Expr.Paren
                (this as Node).children(anns, " ")
                if (shouldAddParens) app.append("()")
                app.append(' ')
            }
        }

    // Ends with newline if last is ann or space is last is mod or nothing if empty
    protected fun Node.WithModifiers.childMods(newlines: Boolean = true) =
        (this@childMods as Node).childMods(mods, newlines)

    protected fun Node.childMods(mods: List<Node.Modifier>, newlines: Boolean = true) =
        this@Writer.also {
            if (mods.isNotEmpty()) {
                this@childMods.apply {
                    mods.forEachIndexed { index, mod ->
                        children(mod)
                        if (newlines &&
                            (mod is Node.Modifier.AnnotationSet ||
                                mods.getOrNull(index + 1) is Node.Modifier.AnnotationSet)) {
                            app.lineEnd().lineBegin()
                        } else {
                            app.append(' ')
                        }
                    }
                }
            }
        }

    protected fun Node.WithModifiers.childModsBeforeType(ref: Node.TypeRef) =
        this@Writer.also {
            if (mods.isNotEmpty()) {
                // As a special case, if there is a trailing annotation with no args and the ref has a paren which is a paren
                // type or a non-receiver fn type, then we need to add an empty set of parens ourselves
                val lastAnn =
                    (mods.lastOrNull() as? Node.Modifier.AnnotationSet)
                        ?.anns
                        ?.singleOrNull()
                        ?.takeIf {
                            it.args.isEmpty()
                        }
                fun checkRecieverType(ref: Node.TypeRef.Func): Boolean {
                    val receiverType = ref.receiverType
                    return receiverType!!.ref is Node.TypeRef.Paren
                }
                val shouldAddParens =
                    lastAnn != null &&
                        (ref is Node.TypeRef.Paren ||
                            (ref is Node.TypeRef.Func &&
                                (ref.receiverType == null || checkRecieverType(ref))))
                (this as Node).children(mods, " ")
                if (shouldAddParens) app.append("()")
                app.append(' ')
            }
        }

    fun Node.children(v: Node?) = this@Writer.also {
        visitChildren(v)
    }

    // Null list values are asterisks
    protected fun Node.bracketedChildren(v: List<Node?>, appendIfNotEmpty: String = "") =
        this@Writer.also {
            if (v.isNotEmpty()) {
                app.append('<')
                v.forEachIndexed { index, node ->
                    if (index > 0) app.append(", ")
                    if (node == null) app.append('*') else children(node)
                }
                app.append('>').append(appendIfNotEmpty)
            }
        }

    protected fun Node.parenChildren(v: List<Node?>, newline: Boolean) =
        this@Writer.also {
            if (newline) {
                childrenNewLine(v, ",", "(", ")")
            } else {
                childrenSameLine(v, ", ", "(", ")")
            }
        }

    protected fun Node.childrenLines(v: Node?) = this@Writer.also {
        if (v == null) {
            return@also
        }
        childrenLines(listOf(v))
    }

    protected fun Node.childrenLines(v: List<Node?>) = this@Writer.also {
        v.forEachIndexed { index, node ->
            app.lineBegin().also {
                children(node)
            }
            if (stmtRequiresEmptyBraceSetBeforeLineEnd(node, v.getOrNull(index + 1))) {
                app.append(" {}")
            }
            if (stmtRequiresSemicolonSetBeforeLineEnd(node, v.getOrNull(index + 1))) {
                app.append(';')
            }

            app.line()
        }
    }

    protected fun stmtRequiresEmptyBraceSetBeforeLineEnd(v: Node?, next: Node?): Boolean {
        // As a special case, if this is a local memberless class decl stmt and the next line is a paren
        // or ann+paren, we have to explicitly provide an empty brace set
        // See: https://youtrack.jetbrains.com/issue/KT-25578
        // TODO: is there a better place to do this?
        fun checkEmpty(v: Node.Stmt.Decl): Boolean {
            val decl = v.decl as Node.Decl.Structured
            return decl.members.isNotEmpty()
        }
        fun checkForm(v: Node.Stmt.Decl): Boolean {
            val decl = v.decl as Node.Decl.Structured
            return decl.form != Node.Decl.Structured.Form.CLASS
        }
        if (v !is Node.Stmt.Decl ||
            v.decl !is Node.Decl.Structured ||
            checkEmpty(v) ||
            checkForm(v)) {
            return false
        }
        fun checkNext(expr: Node.Expr.Annotated): Boolean {
            return expr.expr !is Node.Expr.Paren
        }
        if (next !is Node.Stmt.Expr ||
            (next.expr !is Node.Expr.Paren &&
                (next.expr !is Node.Expr.Annotated ||
                    checkNext(next.expr as Node.Expr.Annotated)))) {
            return false
        }
        return true
    }

    protected fun stmtRequiresSemicolonSetBeforeLineEnd(v: Node?, next: Node?) =
        stmtHasModifierLocalVarDeclAmbiguity(v, next) ||
            stmtHasTrailingLambdaAmbiguity(v, next)

    protected fun stmtHasModifierLocalVarDeclAmbiguity(v: Node?, next: Node?): Boolean {
        // As a special case, if there is just a name stmt, and it is a modifier, and the next stmt is
        // a decl, we need a semicolon
        // See: https://youtrack.jetbrains.com/issue/KT-25579
        // TODO: is there a better place to do this
        if (v !is Node.Stmt.Expr || v.expr !is Node.Expr.Name || next !is Node.Stmt.Decl) {
            return false
        }
        val name = (v.expr as Node.Expr.Name).name.toUpperCase()
        return Node.Modifier.Keyword.values().any {
            it.name == name
        }
    }

    protected fun stmtHasTrailingLambdaAmbiguity(v: Node?, next: Node?): Boolean {
        // As a special case, if there is a function call stmt w/ no trailing lambda followed by a brace
        // stmt, the call needs a semicolon
        if (v !is Node.Stmt.Expr ||
            v.expr !is Node.Expr.Call ||
            (v.expr as Node.Expr.Call).lambda != null) {
            return false
        }
        return next is Node.Stmt.Expr && next.expr is Node.Expr.Brace
    }

    protected fun Node.childrenNewLine(
        v: List<Node?>,
        sep: String,
        prefix: String,
        postfix: String
    ) = this@Writer.also {
        app.append(prefix)
        if (v.size > 0) {
            app.lineEnd().indented {
                v.forEachIndexed { index, t ->
                    app.lineBegin()
                    visit(t, this)
                    if (index < v.size - 1) app.append(sep)
                    app.lineEnd()
                }
            }
            app.lineBegin()
        }
        app.append(postfix)
    }

    protected fun Node.childrenSameLine(
        v: List<Node?>,
        sep: String,
        prefix: String,
        postfix: String
    ) = this@Writer.also {
        app.append(prefix)
        v.forEachIndexed { index, t ->
            visit(t, this)
            if (index < v.size - 1) app.append(sep)
        }
        app.append(postfix)
    }

    protected fun Node.children(
        v: List<Node?>,
        sep: String = "",
        prefix: String = "",
        postfix: String = "",
        newline: Boolean = false
    ) = this@Writer.also {
        if (newline) {
            childrenNewLine(v, sep, prefix, postfix)
        } else {
            childrenSameLine(v, sep, prefix, postfix)
        }
    }

    companion object {
        fun write(v: Node, extrasManager: ExtrasManager, lineLengthLimit: Int): String =
            Writer(extrasManager, lineLengthLimit = lineLengthLimit).run {
                write(v)
                app.toString()
            }
    }
}

private fun StringBuilder.getLastLine(): StringBuilder {
    val index = lastIndexOf("\n")
    return if (index == -1) {
        StringBuilder(this)
    } else {
        StringBuilder(substring(index, length))
    }
}

class LineLengthLimitExceeded : Exception("Line length limit exceeded")

private fun CharSequence.hasMultipleLines(): Boolean = indexOf('\n') > -1
