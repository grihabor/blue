package com.github.grihabor.blue

import java.lang.StringBuilder

class FileBuilder(
    val beforeLineEnd: (fb: FileBuilder) -> Unit,
    var startIndent: String = "",
    val indentation: String = "    "
) {
    private val sb = StringBuilder()
    var indent: String = startIndent

    fun <T> indented(fn: () -> T): FileBuilder = run {
        indent += indentation
        fn()
        return also {
            indent = indent.dropLast(4)
        }
    }

    fun isEmpty(): Boolean = sb.isEmpty()

    fun removeLatestLine(): String {
        val latestLineStart = sb.lastIndexOf("\n")
        if (latestLineStart == -1) {
            val latestLine = sb.toString()
            sb.clear()
            return latestLine
        }
        val latestLine = sb.substring(latestLineStart, sb.length)
        sb.delete(latestLineStart, sb.length)
        return latestLine
    }

    private fun endLine() = also {
        beforeLineEnd(this)
        append('\n')
    }

    fun lineEnd(str: String = "") = append(str).endLine()
    fun line() = endLine()
    fun lineBegin(str: String = "") = also {
        sb.append(indent).append(str)
    }
    fun append(ch: Char) = also {
        sb.append(ch)
    }
    fun append(str: String) = also {
        sb.append(str)
    }
    fun appendName(name: String) =
        if (name.shouldEscapeIdent) append("`$name`") else append(name)
    fun appendNames(names: List<String>, sep: String) = also {
        names.forEachIndexed { index, name ->
            if (index > 0) append(sep)
            appendName(name)
        }
    }

    // We accept lots of false positives to be simple and not have to bring in JVM dep to do accurate check
    protected val String.shouldEscapeIdent
        get() = KEYWORDS.contains(this) || all {
            it == '_'
        } || first() in '0'..'9' || any {
            it !in 'a'..'z' && it !in 'A'..'Z' && it !in '0'..'9' && it != '_'
        }

    companion object {
        protected val KEYWORDS =
            setOf(
                "as",
                "break",
                "class",
                "continue",
                "do",
                "else",
                "false",
                "for",
                "fun",
                "if",
                "in",
                "interface",
                "is",
                "null",
                "object",
                "package",
                "return",
                "super",
                "this",
                "throw",
                "true",
                "try",
                "typealias",
                "typeof",
                "val",
                "var",
                "when",
                "while"
            )
    }

    override fun toString(): String {
        return sb.toString()
    }
}
