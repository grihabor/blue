package com.github.grihabor.blue

import com.github.grihabor.blue.kastree.ExtrasMap
import com.github.grihabor.blue.kastree.Node
import java.lang.RuntimeException

enum class Policy {
    Keep,
    ForceOne,
    ForceZero,
    ForceBasedOnComments
}

class ExtrasManager(
    val extrasMap: ExtrasMap
) : ExtrasMap {

    var blankLinesAfter = HashMap<Node, Policy>()
    var blankLinesBefore = HashMap<Node, Policy>()
    var blankLinesWithin = HashMap<Node, Policy>()

    private fun convertExtras(extras: List<Node.Extra>, policy: Policy): List<Node.Extra> =
        if (policy == Policy.Keep) {
            extras
        } else if (policy == Policy.ForceZero && extras.isNotEmpty()) {
            extras.filter {
                it !is Node.Extra.BlankLines
            }
        } else if (policy == Policy.ForceOne && extras.isNotEmpty()) {
            extras.map {
                when (it) {
                    is Node.Extra.Comment -> it
                    is Node.Extra.BlankLines -> Node.Extra.BlankLines(1)
                }
            }
        } else if (policy == Policy.ForceZero && extras.isEmpty()) {
            emptyList()
        } else if (policy == Policy.ForceOne && extras.isEmpty()) {
            listOf(Node.Extra.BlankLines(1))
        } else if (policy == Policy.ForceBasedOnComments && extras.any {
            it is Node.Extra.Comment
        }) {
            convertExtras(extras, Policy.ForceOne)
        } else if (policy == Policy.ForceBasedOnComments && !extras.any {
            it is Node.Extra.Comment
        }) {
            convertExtras(extras, Policy.ForceZero)
        } else {
            throw RuntimeException("Unexpected case: extras=$extras policy=$policy")
        }

    override fun extrasAfter(v: Node): List<Node.Extra> {
        val extras = extrasMap.extrasAfter(v)
        val lineCount = blankLinesAfter.get(v)
        return convertExtras(extras, lineCount ?: Policy.Keep)
    }

    override fun extrasBefore(v: Node): List<Node.Extra> {
        val extras = extrasMap.extrasBefore(v)
        val lineCount = blankLinesBefore.get(v)
        return convertExtras(extras, lineCount ?: Policy.Keep)
    }
    override fun extrasWithin(v: Node): List<Node.Extra> {
        val extras = extrasMap.extrasWithin(v)
        val lineCount = blankLinesWithin.get(v)
        return convertExtras(extras, lineCount ?: Policy.Keep)
    }

    fun forceBlankLinesAfter(v: Node, policy: Policy) {
        blankLinesAfter[v] = policy
    }

    fun forceBlankLinesBefore(v: Node, policy: Policy) {
        blankLinesBefore[v] = policy
    }

    fun forceBlankLinesWithin(v: Node, policy: Policy) {
        blankLinesWithin[v] = policy
    }
}
