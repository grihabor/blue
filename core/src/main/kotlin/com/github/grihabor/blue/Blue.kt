package com.github.grihabor.blue

import com.github.grihabor.blue.kastree.Converter
import com.github.grihabor.blue.kastree.Parser
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.text.Regex
import java.util.regex.Pattern

private fun getFileContent(filename: String): String {
    val bytes = Files.readAllBytes(Paths.get(filename))
    return bytes.toString(Charsets.UTF_8)
}

private fun replaceFileContent(filename: String, content: String) {
    File(filename).writeText(content)
}

fun getFormattedCode(code: String, lineLengthLimit: Int): String {
    val extrasMap = Converter.WithExtras()
    val parsed = Parser(extrasMap).parseFile(code)
    val extrasManager = ExtrasManager(extrasMap)
    return Writer.write(parsed, extrasManager, lineLengthLimit = lineLengthLimit)
}

fun reformatInplace(filename: String, lineLengthLimit: Int): Boolean {
    val code = getFileContent(filename)
    val formatted = getFormattedCode(code, lineLengthLimit)
    if (code == formatted) {
        return false
    }
    replaceFileContent(filename = filename, content = formatted)
    return true
}

fun searchFiles(filename: String, pattern: String): List<String> =
    File(filename).walk().filter {
        it.name.matches(Regex(pattern))
    }.map {
        it.toString()
    }.toList()

fun searchFiles(dirs: List<String>, pattern: String): List<String> = dirs.map {
    searchFiles(it, pattern)
}.flatten()

fun reformatInplace(
    filenames: List<String>,
    lineLengthLimit: Int = 90,
    verbose: Boolean = false
): Boolean {
    var anyFileChanged = false
    for (filename: String in filenames) {
        try {
            val fileChanged = reformatInplace(filename, lineLengthLimit)
            if (fileChanged) {
                anyFileChanged = true
                System.err.println("Reformatted $filename")
            } else if (verbose) {
                System.err.println("OK: $filename")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    return anyFileChanged
}

fun checkFormatting(filename: String, lineLengthLimit: Int): Boolean {
    val code = getFileContent(filename)
    val formatted = getFormattedCode(code.trimIndent(), lineLengthLimit)
    return code == formatted
}

fun checkFormatting(
    filenames: List<String>,
    lineLengthLimit: Int = 90,
    verbose: Boolean = false
): Int {
    var failCount = 0
    for (filename: String in filenames) {
        val result = checkFormatting(filename, lineLengthLimit)
        if (!result) {
            failCount += 1
            System.err.println("Not formatted: $filename")
        } else if (verbose) {
            System.err.println("OK: $filename")
        }
    }
    return failCount
}

const val BLUE_DEFAULT_PATTERN = ".*.kt$"

fun printAllFilesOK() {
    System.err.println("All files formatted correctly!")
}
