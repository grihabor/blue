fun foo() {
    requireContext().let { context ->
        this.stats.summary = getString(
            R.string.stat_summary,
            getString(
                R.string.speed,
                Formatter.formatFileSize(context, stats.txRate)
            ),
            getString(
                R.string.speed,
                Formatter.formatFileSize(context, stats.rxRate)
            ),
            Formatter.formatFileSize(context, stats.txTotal),
            Formatter.formatFileSize(context, stats.rxTotal)
        )
    }
}