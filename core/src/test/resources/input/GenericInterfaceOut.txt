
interface Source<out T> {
    fun nextT(): T
}
