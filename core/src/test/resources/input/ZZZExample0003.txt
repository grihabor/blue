private object URLSorter : BaseSorter<URL>() {
    private val ordering = compareBy<URL>({ it.host }, { it.port }, { it.file }, { it.protocol })
    override fun compareNonNull(o1: URL, o2: URL): Int = ordering.compare(o1, o2)
}
