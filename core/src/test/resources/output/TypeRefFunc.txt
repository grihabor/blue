fun foo() {
    val bar: (
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz,
        Baz
    ) -> Unit =
        { x: Baz ->
            println(x)
        }
}
