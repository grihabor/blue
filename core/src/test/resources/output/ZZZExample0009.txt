package com.github.grihabor.blue

import com.jfrog.bintray.gradle.BintrayExtension
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.publish.PublishingExtension
import org.gradle.api.publish.maven.MavenPublication
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.bundling.Jar
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

open class BlueCommonPluginExtension {
    var groupId: String? = null
    var artifactId: String? = null
    var version: String? = null
    var user: String? = null
    var key: String? = null
    var vcsUrl: String? = null
    var description: String? = null
}

class BlueCommonPlugin : Plugin<Project> {
    override fun apply(project: Project) {

        val extension =
            project.extensions.create("blueCommon", BlueCommonPluginExtension::class.java)
        val sourcesJarTask = "sourcesJar"

        project.tasks.register<Jar>(sourcesJarTask, Jar::class.java) {
            it.archiveClassifier.set("sources")
            val sourceSets = project.extensions.getByType(SourceSetContainer::class.java)
            val main = sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)
            it.from(main.allJava)
        }

        project.afterEvaluate {
            project.extensions.configure(PublishingExtension::class.java) {
                it.publications.create("blueToolJava", MavenPublication::class.java) {
                    it.apply {
                        from(project.components.getByName("java"))
                        artifact(project.tasks.getByName(sourcesJarTask))
                        groupId = extension.groupId!!
                        artifactId = extension.artifactId!!
                        version = extension.version!!
                    }
                }
            }

            project.extensions.configure(BintrayExtension::class.java) {
                it.apply {
                    user = extension.user
                    key = extension.key
                    setPublications("blueToolJava")
                    pkg.apply {
                        repo = "maven"
                        name = listOf(extension.groupId!!, extension.artifactId!!).joinToString(
                            ":"
                        )
                        setLicenses("MIT")
                        vcsUrl = extension.vcsUrl!!
                        version.apply {
                            name = extension.version!!
                            desc = extension.description!!

                            val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ"
                            val formatter = DateTimeFormatter.ofPattern(pattern)
                            val zoneId = ZoneId.of("Europe/Moscow")
                            released = ZonedDateTime.of(LocalDateTime.now(), zoneId).format(
                                formatter
                            )
                        }
                    }
                }
            }
        }
    }
}
