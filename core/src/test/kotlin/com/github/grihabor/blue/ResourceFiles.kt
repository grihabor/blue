import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

@Throws(IOException::class)
fun getResourceFiles(path: String): List<String> = getResourceAsStream(path).use {
    return if (it == null) {
        emptyList()
    } else {
        BufferedReader(InputStreamReader(it)).readLines()
    }
}

private fun getResourceAsStream(resource: String): InputStream? =
    Thread.currentThread().contextClassLoader.getResourceAsStream(resource) ?: resource::class.java.getResourceAsStream(
        resource
    )

fun getResourceContent(path: String): String =
    getResourceAsStream(path)!!.bufferedReader().use {
        it.readText()
    }
