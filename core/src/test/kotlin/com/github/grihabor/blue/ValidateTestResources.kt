package com.github.grihabor.blue

import com.github.grihabor.blue.kastree.Parser
import getResourceFiles
import getResourceContent

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.junit.runners.Parameterized.Parameters
import java.lang.RuntimeException
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

@RunWith(Parameterized::class)
class ValidateTestResources(
    val filenameExamples: String,
    val contentExamples: String,
    val filenameExpected: String,
    val contentExpected: String
) {

    companion object {

        const val INPUT = "/input"
        const val OUTPUT = "/output"

        val FILTER = null // "IfBodyWithAnnotation.txt"

        @JvmStatic
        @Parameters
        fun data(): Collection<Array<Any>> {
            val files = getResourceFiles(OUTPUT)
            val filtered = if (FILTER == null) {
                files
            } else {
                files.filter {
                    it == FILTER
                }
            }

            val pairs = filtered.map { filename ->
                val expected_path = String.format("$OUTPUT/%s", filename)
                val examples_path = String.format("$INPUT/%s", filename)
                return@map arrayOf(examples_path, // filenameExamples
                getResourceContent(examples_path), // contentExamples
                expected_path, // filenameExpected
                getResourceContent(expected_path))
            }
            val pairs_ = pairs.filterIsInstance<Array<Any>>()
            if (pairs.size != pairs_.size) {
                throw RuntimeException(
                    "Collection items are expected to be of an Array type"
                )
            }
            return pairs_
        }
    }

    fun multilineTrimEnd(content: String): String {
        return content.split('\n').map { line ->
            line.trimEnd()
        }.joinToString(separator = "\n")
    }

    /**
     * The test checks [OUTPUT] files for trailing spaces absence
     */
    @Test
    fun noTrailingSpacesInExpectedContent() {
        val trimmedContent = multilineTrimEnd(contentExpected)
        val prefix = "// ${filenameExpected}\n"
        val expected = prefix + trimmedContent
        val actual = prefix + contentExpected
        assertEquals(expected = expected, actual = actual)
    }

    /**
     * The test checks if blue tool works as expected
     */
    @Test
    fun blueToolReturnsCorrectlyFormattedCode() {
        val formatted = try {
            getFormattedCode(contentExamples, lineLengthLimit = 90)
        } catch (e: Parser.ParseError) {
            e.file.name = filenameExamples
            throw e
        }
        val prefix = "// ${filenameExpected}\n"
        val expected = prefix + multilineTrimEnd(contentExpected)
        val actual = prefix + formatted
        assertEquals(expected = expected, actual = actual)
    }

    /**
     * Checks if the user accidentally placed formatted code
     * into [OUTPUT] directory. To prevent that it compares
     * [INPUT] and [OUTPUT] files content for equality
     */
    @Test
    fun expectedOutputIsDifferentFromInput() {
        val prefix = "// ${filenameExamples}\n"
        val illegal = prefix + contentExpected
        val actual = prefix + contentExamples
        assertNotEquals(illegal = illegal, actual = actual)
    }
}
