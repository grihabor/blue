package com.github.grihabor.blue

import com.github.grihabor.blue.ValidateTestResources.Companion.INPUT
import com.github.grihabor.blue.ValidateTestResources.Companion.OUTPUT
import getResourceFiles
import org.junit.Test
import kotlin.test.assertEquals

class MatchTestResources {
    /**
     * Checks for [INPUT] and [OUTPUT] dirs consistency
     */
    @Test
    fun MatchTestResources() {
        val expected = getResourceFiles(OUTPUT).toSet()
        val examples = getResourceFiles(INPUT).toSet()
        assertEquals(expected, examples)
    }
}
