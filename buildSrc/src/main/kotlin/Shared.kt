class Blue {
    companion object {
        const val vcsUrl = "https://gitlab.com/grihabor/blue/"
        const val checkBlueDescription = "Checks files formatting with blue tool"
        const val formatBlueDescription = "Reformats files in place with blue tool"
        const val kotlinVersion = "1.3.31"

        val version by lazy {
            val process =
                ProcessBuilder("git", "describe", "--tags", "--always", "--first-parent")
                    .start()
            val describe = process.inputStream.reader(Charsets.UTF_8).readText().trim()

            val describeRegex =
                Regex("v([\\d]+)\\.([\\d]+)\\.([\\d]+)(-([\\d]+)-[a-z\\d]+)?")
            val result = describeRegex.matchEntire(describe)!!
            assert(result.groupValues.size == 4 || result.groupValues.size == 6)
            val major = result.groupValues[1]
            val minor = result.groupValues[2]
            val micro = result.groupValues[3]
            if (result.groupValues.size == 4 || result.groupValues[5] == "") {
                "$major.$minor.$micro"
            } else {
                val buildNumber = result.groupValues[5]
                "$major.$minor.$micro-a$buildNumber"
            }
        }
    }
}
