
plugins {
    // the plugin version does not matter in buildSrc
    kotlin("jvm") version "1.3.31"  
    id("java-gradle-plugin")
}

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    compileOnly(gradleApi())
    implementation("com.jfrog.bintray.gradle:gradle-bintray-plugin:1.8.4")
    implementation(kotlin("stdlib-jdk8"))
}


gradlePlugin {
    plugins.register("blueCommonPlugin") {
        id = "com.github.grihabor.blue-common-plugin"
        implementationClass = "com.github.grihabor.blue.BlueCommonPlugin"
    }
}
