package com.github.grihabor.blue

import org.gradle.testkit.runner.GradleRunner
import org.junit.Test
import java.io.File
import kotlin.test.assertEquals

import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.junit.Before
import java.nio.file.Files
import java.nio.file.Paths

class TestBluePlugin {
    var tempDir: File? = null
    @Before
    fun initialize() {
        tempDir = createTempDir()
        File(tempDir, "build.gradle").writeText("""
plugins {
    id "com.github.grihabor.blue"
}
""")
        File(tempDir, "Main.kt").writeText("""
fun main() { if (day) { println("It's a day") } else { println("It's night") } }
""")
        File(tempDir, "gradle.settings").writeText("""
rootProject.name = 'testProject'
""")
    }
    @Test
    fun testTasks() {

        val result =
            GradleRunner
                .create()
                .withProjectDir(tempDir)
                .withArguments("tasks")
                .withPluginClasspath()
                .build()
        assertEquals("Formatting tasks", result.output.split("\n")[12])
        assertEquals(
            "formatBlue - Reformats files in place with blue tool",
            result.output.split("\n")[14]
        )
        assertEquals(SUCCESS, result.task(":tasks")!!.outcome)
    }

    @Test
    fun testFormatBlue() {
        val taskName = "formatBlue"
        val result =
            GradleRunner
                .create()
                .withProjectDir(tempDir)
                .withArguments(taskName)
                .withPluginClasspath()
                .build()
        val lines = result.output.split("\n")
        assertEquals("", lines[0])
        assertEquals("> Task :${taskName}", lines[1])
        assertEquals("Reformatted", lines[2].split(" ")[0])
        assertEquals("All files formatted correctly!", lines[3])
        assertEquals("", lines[4])
        assertEquals("BUILD", lines[5].split(" ")[0])
        assertEquals("SUCCESSFUL", lines[5].split(" ")[1])
        assertEquals(SUCCESS, result.task(":${taskName}")!!.outcome)
        val content =
            Files
                .readAllBytes(Paths.get(File(tempDir, "Main.kt").toString()))
                .toString(Charsets.UTF_8)
        assertEquals("""fun main() {
    if (day) {
        println("It's a day")
    } else {
        println("It's night")
    }
}
""", content)
    }

    @Test
    fun testCheckBlue() {
        val taskName = "checkBlue"
        val result =
            GradleRunner
                .create()
                .withProjectDir(tempDir)
                .withArguments(taskName)
                .withPluginClasspath()
                .buildAndFail()
        assertEquals(FAILED, result.task(":${taskName}")!!.outcome)
    }
}
