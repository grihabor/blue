package com.github.grihabor.blue

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.util.*

open class BluePluginExtension {
    var filenames: Array<String> = emptyArray()
    var pattern = BLUE_DEFAULT_PATTERN
    var verbose = false
    var lineLengthLimit = 90
}

internal const val FORMATTING_GROUP = "Formatting"
internal const val CHECK_GROUP = "Verification"

class BluePlugin : Plugin<Project> {
    private val properties: Properties
        get() {
            val properties = Properties()
            val stream =
                BluePlugin::class.java.getResource("/plugin.properties").openStream()
            properties.load(stream)
            return properties
        }

    override fun apply(project: Project) {
        val extension = project.extensions.create("blue", BluePluginExtension::class.java)
        project.task("formatBlue") {
            it.group = FORMATTING_GROUP
            it.description = properties["formatBlueDescription"]!!.toString()
            it.doLast {
                val filenames = if (extension.filenames.isEmpty()) {
                    listOf(project.rootDir.absolutePath)
                } else {
                    extension.filenames.toList()
                }
                reformatInplace(
                    searchFiles(filenames, extension.pattern),
                    extension.lineLengthLimit,
                    extension.verbose
                )
                printAllFilesOK()
            }
        }
        project.task("checkBlue") {
            it.group = CHECK_GROUP
            it.description = properties["checkBlueDescription"]!!.toString()
            it.doLast {
                val filenames = if (extension.filenames.isEmpty()) {
                    listOf(project.rootDir.absolutePath)
                } else {
                    extension.filenames.toList()
                }
                val failCount =
                    checkFormatting(
                        searchFiles(filenames, extension.pattern),
                        extension.lineLengthLimit,
                        extension.verbose
                    )
                if (failCount > 0) {
                    throw GradleException("Fail count: $failCount")
                } else {
                    printAllFilesOK()
                }
            }
        }
    }
}
