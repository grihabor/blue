import java.util.Properties
plugins {
    kotlin("jvm") version Blue.kotlinVersion
    id("java-gradle-plugin")
    id("maven-publish")
    id("com.gradle.plugin-publish") version "0.10.0"
    id("jacoco")
    id("com.jfrog.bintray")
    id("com.github.grihabor.blue-common-plugin")
}


val pluginDisplayName = "Blue formatter plugin"

gradlePlugin {
    plugins.register("bluePlugin") {
        id = "com.github.grihabor.blue"
        displayName = pluginDisplayName
        description = "The uncompromising Kotlin code formatter"
        implementationClass = "${project.group}.BluePlugin"
        version = project.version.toString()
    }
}

repositories {
    mavenLocal()
    mavenCentral()
}


dependencies {
    "implementation"(project(":core"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib:${Blue.kotlinVersion}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("junit:junit:4.11")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:${Blue.kotlinVersion}")
}

pluginBundle {
    website = Blue.vcsUrl
    vcsUrl = Blue.vcsUrl
    tags = mutableListOf("kotlin", "formatter", "blue", "linter", "style")
}

tasks.test {
    testLogging.showStandardStreams = true
}

jacoco {
    toolVersion = "0.8.1"
}

tasks.jacocoTestReport {
    reports {
        xml.isEnabled = false
        csv.isEnabled = false
        html.isEnabled = true
    }
}

tasks.jar {
    this.archiveBaseName.set("blue-gradle-plugin")
}

blueCommon {
    groupId = project.group.toString()
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_API_KEY")
    version = project.version.toString()
    artifactId = "com.github.grihabor.blue.gradle.plugin"
    description = "Blue formatting plugin"
}


val generatedPropertiesDir = "${project.buildDir}/generated-properties"
val propertiesTaskName = "generateVersionProperties"

sourceSets {
    main {
        output.dir(mapOf(Pair("builtBy", propertiesTaskName)), generatedPropertiesDir)
    }
}

task<Task>(propertiesTaskName){
    doLast {
        val propertiesFile = file("$generatedPropertiesDir/plugin.properties")
        propertiesFile.parentFile.mkdirs()
        val properties = Properties()
        properties.setProperty("checkBlueDescription", Blue.checkBlueDescription)
        properties.setProperty("formatBlueDescription", Blue.formatBlueDescription)
        propertiesFile.writer().let { properties.store(it, null) }
    }
}

tasks.named<Task>("processResources"){
    dependsOn(propertiesTaskName)
}
